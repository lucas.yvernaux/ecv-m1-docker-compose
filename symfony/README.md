# Docker Compose
## Symfony
### Requirements
- MariaDB 10: an available MariaDB server running on port `3306` and host `db` with the username as `symfony`, the password as `symfony` and a database named `symfony`.

### How to run
Copy the environment file
```sh
cp .env .env.local
```

Install dependencies with composer
```sh
composer install
```

Create the database if it does not exist
```sh
php bin/console doctrine:database:create --if-not-exists
```

Create the database schema
```sh
php bin/console doctrine:schema:update
```

### Part exercise student
This commands are already execute in the container docker_symfony(=app). So, we don't need to launch it (See Dockerfile)
We have to execute the following commande only : 
```sh
docker-compose build
docker-compose up
```
Then, we can go on localhost:8080 to see the Acceuil page or go localhost:8081 to see the database with following credential
> System:          ` MySQL `
> Serveur:         ` db `
> Utilisateur:     ` symfony `
> Mot de passe:    ` symfony `
> Base de données: `  `
